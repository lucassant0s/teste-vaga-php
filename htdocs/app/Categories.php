<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = ['title', 'status', 'description'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $table = 'categories';

    public function posts()
    {
        return $this->hasMany(Posts::class);
    }
}
