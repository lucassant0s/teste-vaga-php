<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = ['title', 'status', 'description',  'cover_image'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $table = 'posts';

    public function categories()
    {
        return $this->belongsToMany(Categories::class);
    }
}
