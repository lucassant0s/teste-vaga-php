<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'title' => 'Design',
                'status' => true,
                'user_id' => 2,
                'description' => 'Marketing ou mercadologia ou, mais raramente, mercância, é a arte de explorar, criar e entregar valor para satisfazer as necessidades do mercado.'
            ],
            [
                'title' => 'Programação',
                'status' => true,
                'user_id' => 2,
                'description' => 'Marketing ou mercadologia ou, mais raramente, mercância, é a arte de explorar, criar e entregar valor para satisfazer as necessidades do mercado.'
            ],
            [
                'title' => 'Marketing',
                'status' => false,
                'user_id' => 2,
                'description' => 'Marketing ou mercadologia ou, mais raramente, mercância, é a arte de explorar, criar e entregar valor para satisfazer as necessidades do mercado.'
            ]
        ]);
    }
}
