<?php

use Illuminate\Database\Seeder;

class CategoriesPostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories_posts')->insert([
            [
                'category_id' => 3,
                'post_id' => 6
            ],
            [
                'category_id' => 3,
                'post_id' => 7
            ],
            [
                'category_id' => 4,
                'post_id' => 8
            ],
            [
                'category_id' => 5,
                'post_id' => 8
            ],
            [
                'category_id' => 4,
                'post_id' => 9
            ],
            [
                'category_id' => 5,
                'post_id' => 10
            ],
            [
                'category_id' => 5,
                'post_id' => 10
            ],
            [
                'category_id' => 3,
                'post_id' => 11
            ]
        ]);
    }
}
